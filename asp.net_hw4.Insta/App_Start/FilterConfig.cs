﻿using System.Web;
using System.Web.Mvc;

namespace asp.net_hw4.Insta
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
