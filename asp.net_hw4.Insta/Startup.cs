﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(asp.net_hw4.Insta.Startup))]
namespace asp.net_hw4.Insta
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
