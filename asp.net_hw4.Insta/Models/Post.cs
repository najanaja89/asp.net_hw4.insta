﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace asp.net_hw4.Insta.Models
{
    public class Post
    {
        public int Id { get; set; }
        public string Comment { get; set; }
        public string Imagepath { get; set; }
        public int LikeCount { get; set; } = 0;
    }
}